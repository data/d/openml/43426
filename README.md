# OpenML dataset: Historical-data-on-the-trading-of-cryptocurrencies

https://www.openml.org/d/43426

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is historical data on cryptocurrency tradings for the period from 2016-01-01 to 2021-02-21.
If you enjoy this dataset please upvote so I can see it is popular and I need to update it.
Thank you!
Content
This dataset will be good for data analysis in predicting the price for digital cryptocurrencies.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43426) of an [OpenML dataset](https://www.openml.org/d/43426). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43426/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43426/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43426/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

